{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\newcommand{\\bld}[1]{\\boldsymbol{#1}}$\n",
    "$\\newcommand{\\ket}[1]{\\left|{#1}\\right\\rangle}$\n",
    "$\\newcommand{\\bra}[1]{\\left\\langle{#1}\\right|}$\n",
    "$\\newcommand{\\braket}[2]{\\left\\langle{#1}\\,\\right|\\left.{#2}\\right\\rangle}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# V. Calculating many-body observables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Theory"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "So far we have seen how to create single-particle states, evolve them and compute the expectation value of single particle observables. In reality, however, the systems we are looking to describe are in some many-body state. Because we are dealing with non-interacting systems, all the information about all observables is contained in the single-particle lesser Greens function, $\\mathbf{G}^<(t, t')$.\n",
    "\n",
    "It was [shown](http://www.sciencedirect.com/science/article/pii/S0370157313003451) that $\\mathbf{G}^<(t, t')$ can be expressed in terms of the time-evolved scattering states (and bound states) of the system under consideration, in the following way:\n",
    "$$\n",
    "    \\mathbf{G}^<(t, t') =\n",
    "    \\sum_l \\sum_\\alpha \\left[ \\int_{BZ} \\frac{dk}{2\\pi} \\frac{\\partial E_\\alpha(k)}{\\partial k}\n",
    "    \\Theta[v_\\alpha(k)]\\, f_l(E_\\alpha(k)) \\bld{\\psi}_{\\alpha k}(t)\\bld{\\psi}^\\dagger_{\\alpha k}(t') \\right]\n",
    "    +\n",
    "    \\sum_n p_n \\bld{\\phi}_{n}(t) \\bld{\\phi}^\\dagger_{n}(t')\n",
    "$$\n",
    "where $\\alpha$ labels the scattering modes in lead $l$, $\\bld{\\psi}_{\\alpha k}$ is the wavefunction of the scattering state with momentum $k$ in lead $l$ and mode $\\alpha$,   $E_\\alpha(k)$ its energy, $v_\\alpha(k)$ its velocity. $\\Theta$ is the Heaviside distribution, $f_l$ the thermal occupation in lead $l$, and the integral is taken over the first Brillouin zone. The $\\bld{\\phi}_n$ are the bound state wavefunctions of the central region, and $p_n$ their occupation probabilies.\n",
    "\n",
    "The $\\Theta[v_\\alpha(k)]$ ensures that we only fill states with positive velocity, which are coming into the system. We are still integrating over a complete set of states due to the fact that we also sum over the leads.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As any observable can be expressed in terms of products of (elements of) $\\mathbf{G}^<(t, t')$, any observable can be be expressed in terms of momentum integrals over time-evolved scattering states. In all the examples below the contribution from initially filled bound states is removed for brevity.\n",
    "\n",
    "A canonical example would be a density (as discussed in section 3), which would be written\n",
    "$$\n",
    "    \\rho_i(t) =     \\sum_{l,\\,\\alpha} \\left[ \\int_{BZ} \\frac{dk}{2\\pi} \\frac{\\partial E_\\alpha(k)}{\\partial k}\n",
    "    \\Theta[v_\\alpha(k)]\\, f_l(E_\\alpha(k)) \\bld{\\psi}^\\dagger_{\\alpha k,\\,i}(t)\\mathbf{M}_i\\bld{\\psi}_{\\alpha k,\\,i}(t) \\right]\n",
    "$$\n",
    "where $\\bld{\\psi}_{\\alpha k,\\,i}$ is the projection of $\\bld{\\psi}_{\\alpha k}$ onto site $i$ and $\\mathbf{M}_i$ is the matrix which defines the density (again, as discussed in section 3).\n",
    "\n",
    "Another would be the associated current\n",
    "$$\n",
    "    J_{ij}(t) =     2 \\sum_{l,\\,\\alpha} \\left[ \\int_{BZ} \\frac{dk}{2\\pi} \\frac{\\partial E_\\alpha(k)}{\\partial k}\n",
    "    \\Theta[v_\\alpha(k)]\\, f_l(E_\\alpha(k)) \\Im\\left(\\bld{\\psi}^\\dagger_{\\alpha k,\\,i}(t)\\mathbf{M}_i\\mathbf{H}_{ij}(t)\\bld{\\psi}_{\\alpha k,\\,j}(t)\\right) \\right]\n",
    "$$\n",
    "\n",
    "The above quantites have the property that they can be expressed in terms of a single $\\mathbf{G}^<(t, t')$ (because they are single-particle observables), and in addition they occur in the equal-time sector, $t = t'$. \n",
    "\n",
    "To calculate current noise, which is a two-particle observable, requires a product of two $\\mathbf{G}^<(t, t')$, and can depend on two times. Consequently its calculation requires two integrals over the Brillouin zone:\n",
    "$$\n",
    "    S_{\\mu\\nu}(t, t') = \\sum_{l,\\alpha} \\sum_{m, \\beta}\n",
    "    \\int_{BZ} \\frac{dk}{2\\pi} \\frac{\\partial E_\\alpha(k)}{\\partial k} \\int_{BZ} \\frac{dk'}{2\\pi}  \\frac{\\partial E_\\beta(k)}{\\partial k} \n",
    "    f_l(E_\\alpha(k))[1 - f_m(E_\\beta(k'))]\\times\n",
    "    I_\\mu(k, k', t)[I_\\nu(k, k', t')]^*\n",
    "$$\n",
    "where $\\mu$ and $\\nu$ are a set of pairs of sites which define an interface through which current flows, and \n",
    "$$\n",
    "I_\\mu(k, k', t) = \\sum_{\\langle i,j\\rangle \\in \\mu}\n",
    "    \\bld{\\psi}^\\dagger_{\\beta k',\\,i}(t)\n",
    "        \\mathbf{H}_{ij}(t)\\mathbf{M}_j\n",
    "        \\bld{\\psi}_{\\alpha k,\\,j}(t) - \n",
    "    \\bld{\\psi}^\\dagger_{\\beta k',\\,j}(t)\n",
    "        \\mathbf{M}_j \\mathbf{H}_{ji}(t)\n",
    "        \\bld{\\psi}_{\\alpha k,\\,i}(t)\n",
    "$$\n",
    "which can be thought of as a two-body generalization of the current flowing through $\\mu$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can thus partition observables into several classes depending on:\n",
    "\n",
    "+ the number of Greens functions we need to multiply together\n",
    "+ the number of time indices (e.g. one-time or two-time)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What tkwant provides"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "tkwant allows arbitrary elements of $\\mathbf{G}^<(t, t')$ to be calculated. Most of the time, however, we are not interested in accessing $\\mathbf{G}^<(t, t')$ directly, but only in computing some observables, such as the examples above. tkwant also provides an easy interface to computing such objects."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" data-toggle=\"collapse\" data-target=\"#info5\">\n",
    "<p>**Note** <i class=\"pull-right fa fa-chevron-down\"></i></p>\n",
    "<div id=\"info5\" class=\"collapse\">\n",
    "The following is probably wrong, but I'm not sure why.\n",
    "In principle we could forgo the sum over the leads and instead integrate over the whole Brillouin zone of one lead, however the integral would then contain contributions from states which are *leaving the system in lead $l$ and mode $\\alpha$ with 100% probability*; these would require an occupation probability factor $(1 - f_l(E))$, as the state is *entering into* the lead and therefore the state in the lead *must not be filled*. It is not necessarily clear how to deal with such states, especially in systems with broken time reversal symmetry. Is it just a case of doing the following?\n",
    "$$\n",
    "    \\mathbf{G}^<(t, t') =\n",
    "    \\sum_\\alpha \\left[ \\int_{BZ} \\frac{dk}{2\\pi} \\frac{\\partial E_\\alpha(k)}{\\partial k}\n",
    "    \\bld{\\psi}_{\\alpha k}(t) \\bld{\\psi}^\\dagger_{\\alpha k}(t')\n",
    "    \\left\\{\\Theta[v_\\alpha(k)]\\, f_l(E_\\alpha(k)) + [1 - f_l(E_\\alpha(k))]\\Theta[v_\\alpha(k)]\\right\\} \\right]\n",
    "$$\n",
    "Where now there is no sum over leads, only a sum over modes in a particular lead.\n",
    "This seems a bit weird. Imagine if we had a very thin lead and a very thick one, then we don't seem to have \"enough states\" in the thin one to be able to fully describe the thick one. We could even expect no hybridization of thick-lead scattering states to thin-lead scattering states, instead only to thin-lead evanescent states.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculating the thermal average of single particle observables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we make the system to study."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%run bean_system.ipy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[]"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "lat, sys = make_system()\n",
    "\n",
    "lead = make_lead(lat)\n",
    "\n",
    "sys.attach_lead(lead)\n",
    "sys.attach_lead(lead.reversed())\n",
    "\n",
    "tsys = sys.finalized()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we define the observables we wish to compute"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def in_disk(site):\n",
    "    x, y = site.pos\n",
    "    return x**2 + y**2 < 3**2\n",
    "\n",
    "def cuts_circle(site_i, site_j):\n",
    "    return in_disk(site_i) and not in_disk(site_j)\n",
    "\n",
    "def across_y_axis(site_i, site_j):\n",
    "    return site_j.pos[0] <= 0 and site_i.pos[0] > 0  # current from left to right\n",
    "\n",
    "# charge on some central disk, and the current flowing out of the disk\n",
    "Q_disk, = tkwant.Density(tsys, s_0, where=in_disk, sum=True)\n",
    "I_disk, = tkwant.Current(tsys, s_0, where=cuts_circle, sum=True)\n",
    "\n",
    "# current flowing accross the centre of the sample, from left to right\n",
    "I_lr = tkwant.Current(tsys, s_0, where=across_y_axis)\n",
    "I_lr_summed = tkwant.Current(tsys, s_0, where=across_y_axis, sum=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now compute thermal averages for these observables, as defined above"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "args = dict(Vg=1., gamma=1.)\n",
    "\n",
    "Q_disk_avg, = tkwant.thermal_average(tsys, Q_disk, args=args)\n",
    "I_disk_avg, =  tkwant.thermal_average(tsys, I_disk, args=args)\n",
    "I_lr_avg, =  tkwant.thermal_average(tsys, I_lr, args=args)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`tkwant.thermal_average` returns an *iterable* of result objects (hence the trailing comma in the assignments!), each of which can be evaluated at different times to get the thermal average for the observables. These can be accessed just like the values returned by evaluating an observable directly on a wavefunction, in [part 3](3_single_particle_observables.ipynb). The sequence `times` specifies when the observable(s) should be evaluated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print Q_disk_avg(0.)\n",
    "print I_disk_avg(0.)\n",
    "print I_lr_avg(0., lat(0, 5), lat(1, 5))\n",
    "\n",
    "print Q_disk_avg(3.)\n",
    "print I_disk_avg(10.)\n",
    "print I_lr_avg(7.)[lat(0, 5), lat[1, 5]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Behind the scenes, `tkwant.thermal_average` uses the time evolution introduced in the [previous tutorial](4_evolving_states.ipynb) to propagate the scattering states (which are eigenstates for $t \\le 0$) forward until we reach the desired time, and then performs the integral over the Brillouin zone."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The currents are zero at $t=0$, as the leads are identical and by default tkwant takes everything in thermal equilibrium at $T=0$ and with $\\mu=0$. We can, if we wish, explicitly specify an occupation function to use in all the leads"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def f_occupation(E):\n",
    "    return 1. if E < 1. else 0.\n",
    "\n",
    "Q_disk, = tkwant.thermal_average(tsys, Q_disk, occupation=f_occupation,\n",
    "                                 args=args, times=range(100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Occupation functions just have a single energy parameter and return the occupation of states at that energy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If each lead needs its own distribution function (to have different chemical potentials or temperatures) then we can also pass a sequence of occupation functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "occ_left = tkwant.fermi_dirac(T=0, mu=1.0)\n",
    "occ_right = tkwant.fermi_dirac(T=0, mu=0.5)\n",
    "\n",
    "Q_avg, = tkwant.thermal_average(tsys, Q_disk, occupation=[occ_left, occ_right],\n",
    "                               args=args, times=range(100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-warning\" data-toggle=\"collapse\" data-target=\"#suggestion1\">\n",
    "<p>**Suggestion** <i class=\"pull-right fa fa-chevron-down\"></i></p>\n",
    "<div id=\"suggestion1\" class=\"collapse\">\n",
    "Such a function is independent of tkwant, only the time dependent part (next) is.\n",
    "Shouldn't such a function really be included into kwant?\n",
    "\n",
    "This way we can have `kwant.thermal_average` which always computes with (instantaneous) eigenstates (i.e. correct in some adiabatic limit) and `tkwant.thermal_average` which computes with the time-evolved states which were eigenstates for $t\\le0$.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Removing redundant calculations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above is all very well and good, but is quite wasteful for a number of reasons. The different calls to `tkwant.thermal_average` each set up separate calculations, even though we are really just interested in extracting different information from the *same* system with the *same* set of parameters.\n",
    "\n",
    "To improve things we should pass all the observables we wish to calculate to a single call to `tkwant.thermal_average`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Q_avg, I_avg, I_lr_avg = tkwant.thermal_average(tsys, Q_disk, I_disk, I_lr, args=args)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The call to `tkwant.thermal_average` will never block. The calls to get the observables at a given time *will*, however, block."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Q_avg, I_avg, I_lr_avg = tkwant.thermal_average(tsys, Q_disk, I_disk, I_lr, args=args)\n",
    "\n",
    "Q_avg(90.)  # blocks "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note, however, that as we can't go back in time, evaluating observables at earlier\n",
    "times will raise an exception:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "I_avg(3.)  # raises exception\n",
    "I_lr_avg(89.)  # raises exception"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can, however, evaluate other observables at the *same* time:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "I_avg(90.) \n",
    "I_lr_avg(90.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "note, however, that evaluating different observables, even at the same time, *may* induce a large amount of recalculation. If the k-space integration does not have an error below the tolerance, extra points will need to be calculated, necessitating more time evolutions etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" data-toggle=\"collapse\" data-target=\"#info2\">\n",
    "<p>**Note** <i class=\"pull-right fa fa-chevron-down\"></i></p>\n",
    "<div id=\"info2\" class=\"collapse\">\n",
    "The momentum grid on which the points are chosen is one which would correctly integrate (to within some accuracy) the observable over the Brillouin zone. Typically this grid will be non-regular, as we will be using some Clenshaw-Curtis or Gauss-Kronrod scheme as our fundamental integrator.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computing Green's functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can calculate Greens functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def circle(centre):\n",
    "    centre = np.array(centre)\n",
    "    return lambda s: np.sum(s.pos - centre)**2) < 3**2\n",
    "\n",
    "\n",
    "g = tkwant.LesserGreensFunction(tsys, where=circle(4, 10))\n",
    "\n",
    "G = tkwant.thermal_average(tsys, g, args=args)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which take *two* times"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "g_1_0 = G(1., 0.)\n",
    "print g_1_0[lat(5, 9), lat[3, 6]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A caveat with these two-time quantities is that both the times must be either:\n",
    "+ in the future with respect to the current state of the calculation\n",
    "+ in the past (or at the present), but only evaluated at times that we have\n",
    "  previously requested\n",
    "  \n",
    "Explicitly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "g = tkwant.LesserGreensFunction(tsys, where=circle(4, 10))\n",
    "G = tkwant.thermal_average(tsys, g, args=args)\n",
    "\n",
    "G(1., 0.)  # ok\n",
    "G(1., 1.)  # ok\n",
    "G(2., 1.)  # ok\n",
    "G(10., 10.)  # ok\n",
    "G(2., 2.)  # ok -- in the past, but we already saved the necessary information\n",
    "G(3., 2.)  # raises exception! We never asked to be evaluated at time 3, and now we\n",
    "           # are too far gone"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the full Greens function is an incredibly memory-intensive object (see note below on memory requirements), it is *imperative* to restrict to a certain submatrix of a modest size (say 100 degrees of freedom) by using the `where` argument and to restrict the times."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you know that the information you require cannot fit into memory, you can tell tkwant to use a HDF5 file behind the scenes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "G = tkwant.thermal_average(tsys, g, args=args, use_hdf5=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computing Two-Body Observables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These are more tricky as the calculation to perform has a different form; two integrations of the Brillouin zone etc. Support for this will certainly not appear in the first version of tkwant, but the interface, at least, is reasonably clear."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For example, calculating the current noise could be done as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def left(i, j):\n",
    "    i.pos[0] > -7 and j.pos[0] <= -7\n",
    "    \n",
    "def right(i, j):\n",
    "    i.pos[0] > 7 and j.pos[0] <= 7\n",
    "    \n",
    "I_left = tkwant.Current(tsys, s_0, where=left, sum=True)\n",
    "I_right = tkwant.Current(tsys, s_0, where=right, sum=True)\n",
    "\n",
    "args = dict(Vg=1., gamma=1.)\n",
    "\n",
    "sp_noise = tkwant.CurrentNoise(tsys, I_left, I_right)\n",
    "\n",
    "noise = tkwant.thermal_average(tsys, sp_noise, args=args,)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and `noise` can be evaluated with two times"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "noise(3., 2.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "two-particle observables are also quite memory intensive objects, so prudent use of `where` is highly recommended."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" data-toggle=\"collapse\" data-target=\"#note3\">\n",
    "<p>**Implementation Note** <i class=\"pull-right fa fa-chevron-down\"></i></p>\n",
    "<div id=\"note3\" class=\"collapse\">\n",
    "If we only need to calculate 2-particle observables then we can use a better 2D adaptative integration algorithm as well as symmetric quadrature rules on triangles, if not then we have to use quadrature rules on rectangles and use a more crude adaptive algorithm (just brute interval bisection) so that we can re-use points for the 1D integrations (single-particle observables). \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" data-toggle=\"collapse\" data-target=\"#info4\">\n",
    "<p>**Note** <i class=\"pull-right fa fa-chevron-down\"></i></p>\n",
    "<div id=\"info4\" class=\"collapse\">\n",
    "The lesser Greens function is provided for completeness, however in most situations it is more efficient to calculate quantities using the observables interface.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" data-toggle=\"collapse\" data-target=\"#info6\">\n",
    "<p>**Memory requirements** <i class=\"pull-right fa fa-chevron-down\"></i></p>\n",
    "<div id=\"info6\">\n",
    "Let's look at some limiting cases. If we have a large system ($10^6$ degrees of freedom), and require $1000$ points in the Brillouin zone, with 10 modes and 2 leads then this means that we need to store $2\\times10^{10}$ complex numbers in order to be able to compute the full green's function *in the equal time sector, for a given value of time*. That's $320\\text{GB}$. Clearly this is far too large. This is why there is a strong requirement to limit the degrees of freedom for which we will calculate the Greens function. If we limit ourselves to $100$ of the $10^6$ degrees of freedom then the Greens function *in the equal time sector, for a given value of time* can be stored in $32\\text{MB}$. So we can reasonably store $1000$ such Greens functions in memory.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inspecting the state of a calculation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "tkwant calculations will frequently take a reasonable amount of time. You can inspect the state of a calculation easily . You can get the object returned by `tkwant.thermal_average`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "task = tkwant.thermal_average(tsys, Q_disk, I_disk, args=args, times=range(100))\n",
    "Q_disk, I_disk = task\n",
    "\n",
    "# or, equivalently\n",
    "Q_disk, I_disk = tkwant.thermal_average(tsys, Q_disk, I_disk, args=args, times=range(100))\n",
    "task = Q_disk.task"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which behaves just like a sequence; you can iterate over it and get the objects that contain the results, as above. But it is actually a smart object which allows you to interact with a running tkwant calculation and query its state:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print task.state"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stopping and restarting calculations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the object called `task` to interact with the tkwant calculation and save the current state of the calculation. This is\n",
    "invaluable if we are doing a long calculation which we may want to continue later (maybe we have limited time on a computing resource)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "task.save('state.hdf5')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also manually abort a calculation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "task.abort()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also (unsurprisingly) load a state from a file and continue with the calculation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "task = tkwant.load_task('state.hdf5')\n",
    "Q_disk, I_disk = task"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`tkwant.load_task` returns the same kind of object as `tkwant.thermal_average` and can hence be iterated over to get the results objects, as above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parallel computations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The calculation of the thermal averages as described above is a very computationally intensive operation. For any system of more than a few degrees of freedom using multiple cores / multiple machines is a necessity, especially considering the possible memory requirements ($\\sim 100\\text{GB}$) to hold the full state of the solver (wavefunction at a given time for each momentum/lead/mode)\n",
    "\n",
    "Using MPI is the standard way of utilizing multiple cores/machines using a message-passing paradigm. As the computations in `tkwant.thermal_average` are loosely coupled (evolving indepedent ODEs), message passing makes the most sense.\n",
    "\n",
    "You should be able to run a tkwant script without any modifications to the logic and exploit multiple cores/machines by using `mpirun`:\n",
    "\n",
    "    mpirun -n 20 python myscript.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`myscript.py`:\n",
    "```python\n",
    "    import tkwant\n",
    "    \n",
    "    ...\n",
    "    \n",
    "    Q_avg, I_avg = tkwant.thermal_average(tsys,\\\n",
    "        Q_disk, I_disk, args=args)\n",
    "    \n",
    "    Qout = [Q_avg(t) for t in range(100)]\n",
    "    \n",
    "    Q_avg.task.save('the_full_calculation.hdf5')\n",
    "        \n",
    "    ...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One could also imagine using other parallel backends, such as ipyparallel, and having the main script running totally separated from the backend.\n",
    "One could then imagine running an interactive session:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    ">>> Q_avg, = tkwant.thermal_average(tsys, Q_disk,  args=args)\n",
    ">>> Q_avg(10.)\n",
    "KeyboardInterrupt\n",
    ">>> Q_avg.task.state\n",
    "State(time=5.0, k_points=[0, 0.0012, 0.0014, ..., 3.1405])\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "where we see that we can `Ctrl-C` to get back the prompt and query the state of the task *without* stopping the task. This is clearly very different to the previous semantics when using MPI. We could then abort the running task, stop and save its progress etc.:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    ">>> Q_avg.task.save('state.hdf5')\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.1+"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
