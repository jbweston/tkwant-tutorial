{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\newcommand{\\bld}[1]{\\boldsymbol{#1}}$\n",
    "$\\newcommand{\\ket}[1]{\\left|{#1}\\right\\rangle}$\n",
    "$\\newcommand{\\bra}[1]{\\left\\langle{#1}\\right|}$\n",
    "$\\newcommand{\\braket}[2]{\\left\\langle{#1}\\,\\right|\\left.{#2}\\right\\rangle}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# VI. Self-consistent many-body evolution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "In [part 5](5_manybody_observables.ipynb) we saw how to calculate the expectation values of many-particle observables using `tkwant.thermal_average`. In this section we will see how we can feed an observable back in to the evolution of the scattering states to obtain *self-consistent evolution*. Initially our wavefunctions evolved according to\n",
    "$$\n",
    "i\\partial \\bld{\\psi}_k(t) = \\mathbf{H}(t)\\bld{\\psi}_k(t)\n",
    "$$\n",
    "now we wish for them to evolve according to\n",
    "$$\n",
    "    i\\partial \\bld{\\psi}_k(t) = \\mathbf{H}(f(\\{\\bld{\\psi}_k(t)\\}, t), t)\\bld{\\psi}_k(t)\n",
    "$$\n",
    "where $f(\\{\\bld{\\psi}_k(t)\\}, t)$ is a function which depends on the scattering states for all values of $k$ (which themselves depend on time) and possibly explicitly on time. This essentially makes the Schrödinger equation *nonlinear*, and should give interesting physics."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As an example we shall concentrate on the simple case where $f(\\{\\bld{\\psi}_k(t)\\}, t)$ is a single-particle observable, e.g. the density:\n",
    "$$\n",
    "    \\rho_i(\\{\\bld{\\psi}_k(t)\\}, t) =     \\sum_{l,\\,\\alpha} \\left[ \\int_{BZ} \\frac{dk}{2\\pi} \\frac{\\partial E_\\alpha(k)}{\\partial k}\n",
    "    \\Theta[v_\\alpha(k)]\\, f_l(E_\\alpha(k)) \\bld{\\psi}^\\dagger_{\\alpha k,\\,i}(t')\\mathbf{M}_i\\bld{\\psi}_{\\alpha k,\\,i}(t) \\right]\n",
    "$$\n",
    "and only the onsite part of the Hamiltonian depends on the density in the following trivial way\n",
    "$$\n",
    "\\mathbf{H}_{ii}(f(\\{\\bld{\\psi}_k(t)\\}), t) = \\mathbf{H}^0_{ii}(t) + \\rho_i \\mathbf{1}\n",
    "$$\n",
    "where $\\mathbf{H}^0_{ii}(t)$ is the unperturbed part of the onsite Hamiltonian (which may still, of course, contain explicit time-dependence) and $\\mathbf{1}$ is the identity matrix over the orbitals on site $i$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can write the following code to implement this"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def onsite(site, gamma, rho):  # no explicit time dependence, but implicit via \"rho\" parameter\n",
    "    H0 = 4 * gamma\n",
    "    V = rho[site] # get charge density at present time\n",
    "    return H0 + V\n",
    "\n",
    "def hopping(site1, site2, gamma):\n",
    "    return -gamma\n",
    "\n",
    "lat = kwant.lattice.square()\n",
    "sys = kwant.Builder()\n",
    "sys[(lat[i, j] for i in xrange(5) for j in xrange(5))] = onsite\n",
    "sys[lat.neighbors()] = hopping\n",
    "\n",
    "tsys = sys.finalized()\n",
    "\n",
    "rho = tkwant.Density(tsys)\n",
    "I = tkwant.CurrentDensity(tsys)\n",
    "\n",
    "I_avg, = tkwant.thermal_average(tsys, I, args=dict(gamma=gamma), times=xrange(100),\n",
    "                                selfconsistent_args=dict(rho=rho))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The only new part is the `selfconsistent_args` which are passed to `tkwant.thermal_average`. We pass in an observable which gets bound to the `rho` parameter in the `onsite` function; what's going on? tkwant takes the single particle observables specified in `selfconsistent_args` and evaluates their thermal average; the result is what is passed in the `rho` parameter to the `onsite` value function.\n",
    "\n",
    "As we are evolving in time, naturally the evaluation of the thermal average needs to happen at subsequent times in order for the correct equations to be solved. Because tkwant knows what it needs to compute it does the right thing and the above code works seamlessly.\n",
    "\n",
    "We see that the above code actually represents a highly non-trivial calculation, but it makes perfect sense at first glance and corresponds closely to the mathematical expression written above.\n",
    "\n",
    "We have also explicitly limited the user to using time-local observables for self-consistent evolution. If we relax this constraint the algorithms become a lot more complicated to implement.\n",
    "\n",
    "It turns out (see the [implementation details](implementation_details/selfconsistent_manybody.ipynb)) that we can optimize the evolution if we remember the value of the self-consistent observable every time the Hamiltonian needs to be evaluated. Say if we have a system with $10^6$ degrees of freedom and $10^5$ evaluations in time (not unreasonable if we wish to go to supercomputers with this thing) that means that we need $100\\text{GB}$ *just* for the observable, not to mention saving the actual current state of the solver... In such a case we can always store parts of the observable to disk and read it out in chunks (it's usage patterns are predictable) and in this way we can avoid overloading memory. If not, then we can always forego the optimization which requires it to be stored."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As another canonical example we shall take the non-linear Schrödinger equation:\n",
    "$$\n",
    "    i\\partial_t \\bld\\psi_{k}(t) = \\mathbf{H}^0(t)\\bld\\psi_k(t) + \\left|\\bld\\psi_k(t)\\right|^2\\bld\\psi_k(t)\n",
    "$$\n",
    "Despite the nonlinearity this is on the surface much *easier* to solve than the previous example because each $k$-point can evolve separately (ignoring the fact that the equation will probably be harder to step in time)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def onsite(site, gamma, psi):\n",
    "    H0 = 4 * gamma # calculation of unperturbed part\n",
    "    # calculation of charge density at present time\n",
    "    V = np.diag(abs(psi[site])**2)\n",
    "    return H0 + V\n",
    "\n",
    "def hopping(site1, site2, gamma):\n",
    "    return -gamma\n",
    "\n",
    "lat = kwant.lattice.square()\n",
    "sys = kwant.Builder()\n",
    "sys[(lat[i, j] for i in xrange(5) for j in xrange(5))] = onsite\n",
    "sys[lat.neighbors()] = hopping\n",
    "\n",
    "tsys = sys.finalized()\n",
    "\n",
    "psi = tkwant.Wavefunction(tsys)\n",
    "I = tkwant.CurrentDensity(tsys)\n",
    "\n",
    "I_avg, = tkwant.thermal_average(tsys, I, args=dict(gamma=gamma), times=xrange(100),\n",
    "                                selfconsistent_args=dict(psi=psi))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By giving a `tkwant.Wavefunction` in the `selfconsistent_args` we tell `tkwant` to bind the `psi` parameter of `onsite` to the wavefunction at the current time. This does not imply an integration over $k$, as integrating the wavefunction over $k$ does not make any sense.\n",
    "\n",
    "We should probably have a way of specifying whether we want to evaluate the true single particle observables or the thermal average... need to think about this."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Yet another canonical example would be Poisson-Schrödinger, where instead of using a naïve model where we add the density directly to the hamiltonian, we solve the Poisson equation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def onsite(site, gamma, V_poiss):\n",
    "    H0 = 4 * gamma # calculation of unperturbed part\n",
    "    # calculation of charge density at present time\n",
    "    V = V_poiss[site]\n",
    "    return H0 + V\n",
    "\n",
    "def hopping(site1, site2, gamma):\n",
    "    return -gamma\n",
    "\n",
    "lat = kwant.lattice.square()\n",
    "sys = kwant.Builder()\n",
    "sys[(lat[i, j] for i in xrange(5) for j in xrange(5))] = onsite\n",
    "sys[lat.neighbors()] = hopping\n",
    "\n",
    "tsys = sys.finalized()\n",
    "\n",
    "rho = tkwant.Density(tsys)\n",
    "I = tkwant.CurrentDensity(tsys)\n",
    "\n",
    "poiss = PoissonSolver(rho)\n",
    "\n",
    "I_avg, = tkwant.thermal_average(tsys, I, args=dict(gamma=gamma), times=xrange(100),\n",
    "                                selfconsistent_args=dict(V_poiss=poiss))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "where \"PoissonSolver\" would be some wrapper around an external Poisson solver. We will need to think a bit about how this will actually happen and what the interfaces are for things passed in \"selfconsistent_args\"."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "IPython 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.1+"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
