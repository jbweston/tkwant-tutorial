{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\newcommand{\\bld}[1]{\\boldsymbol{#1}}$\n",
    "$\\newcommand{\\ket}[1]{\\left|{#1}\\right\\rangle}$\n",
    "$\\newcommand{\\bra}[1]{\\left\\langle{#1}\\right|}$\n",
    "$\\newcommand{\\braket}[2]{\\left\\langle{#1}\\,\\right|\\left.{#2}\\right\\rangle}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The interface for calculating manybody observables is essentially the following"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Q_avg = tkwant.thermal_average(tsys, Q)\n",
    "\n",
    "print Q(1.)\n",
    "print Q(10.)\n",
    "print Q(5.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" data-toggle=\"collapse\" data-target=\"#info1\">\n",
    "<p>**Note on evaluating observables** <i class=\"pull-right fa fa-chevron-down\"></i></p>\n",
    "<div id=\"info1\" class=\"collapse\">\n",
    "Depending on the integration strategy used the \"*evaluated*\" observables mentioned above may either be $k$-resolved (default strategy) or already integrated (precompute strategy). See the section on integration strategies for details.\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dense output from the integrator\n",
    "We want to be able to have results at arbitrary times. It is incredibly inefficient to use a Runge-Kutta method if we need to keep \"stopping\" to get output at densly packed points. What we are looking for is called [dense output](http://www.boost.org/doc/libs/1_58_0/libs/numeric/odeint/doc/html/boost_numeric_odeint/concepts/dense_output_stepper.html) in the literature.\n",
    "\n",
    "For Runge-Kutta methods one usually has to make one or two extra function evaluations in order to construct the dense-output interpolant *of the same order* as the method itself. Adams methods do not require this, as they are themselves contstructed from interpolant schemes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Handling 2-particle observables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If 2-particle and 1-particle observables are requested then an adaptative quadrature method based on rectangles will be employed. This will mean that both types of observables can be calculated using the same $k$-points. In the case where only 2-particle observables are requested then a scheme based on \"simplices\" (triangles) is used. The advantage of such a scheme is that points may be re-used and the method is symmetric in its choice of $k$-points within the fundamental simplices."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initial choice of $k$ intervals\n",
    "We must ensure that the initial choice of $k$ intervals will allow us to avoid any discontinuities in computed quantities. Discontinuities can occur due to 2 causes: $v_\\alpha(k)$ changing sign (so the $\\Theta[v_\\alpha(k)]$ part of the integral comes into play) or $E_\\alpha(k) - E_F$ changing sign (at T=0), so we either neglect or include mode $\\alpha$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Parallel Decomposition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are myriad opportunities for exploitation of parallel execution in tkwant. The main composition techniques will be:\n",
    "\n",
    "+ $k$-space decomposition (independent ODEs in the non-interacting case)\n",
    "+ spatial decomposition (for a single $k$-point)\n",
    "+ time-domain decomposition using a \"parareal\" approach\n",
    "\n",
    "The cases we will have to deal with are the following. In what follows, `nprocs` refers to the number of computation cores available, `n_k` to the number of points in $k$ space (and also the number of modes) required and `N` refers to the system size (total number of degrees of freedom in the final, finite problem to solve).\n",
    "\n",
    "##### I. `nprocs` $\\gg$ `n_k` and `N` \"large\"\n",
    "This is the case for a large system where we don't want or need a $k$-integration. We only want time integration of one or a few $k$ points. This was the case for\n",
    "some of the simulations in the Majorana paper.\n",
    "First decompose over $k$ and then decompose spatially.\n",
    "\n",
    "##### II. `nprocs` $\\gg$ `n_k` and `N` \"small\"\n",
    "This is the case for a large system where we don't want or need a $k$-integration. We only want time integration of one or a few $k$ points. This was the case\n",
    "for some of the simulations in the Majorana paper\n",
    "First decompose over $k$ and then decompose in time using \"parareal\".\n",
    "\n",
    "##### III. `nprocs` $\\approx$ `n_k` and `N` \"large\"\n",
    "We can either only decompose in $k$ and do no spatial nor time decomposition,\n",
    "or we can decompose in $k$ and do spatial decomposition as well, in which\n",
    "case we have to integrate all the $k$-points in \"batches\".\n",
    "\n",
    "\n",
    "##### IV. `nprocs` $\\approx$ `n_k` and `N` \"small\"\n",
    "We can decompose in $k$ and do no spatial nor time decomposition, or we\n",
    "can decompose in $k$ and do some time decomposition, batching $k$-points,\n",
    "as before.\n",
    "\n",
    "##### V. `nprocs` $\\ll$ `n_k` and `N` \"large\"\n",
    "This is currently the most common situation. We want to do an integration over\n",
    "the Brillouin zone and we need lots of $k$-points. Here we will have to use\n",
    "some hybrid methods. If we assume that there are only difficulties in some\n",
    "narrow regions of $k$-space (not unreasonable) then that means that most of the\n",
    "$k$-intervals will remain \"large\" and one or two will recursively split and\n",
    "become \"small\". The difficulty is then that every time we split we have to\n",
    "re-integrate in time for the new $k$-intervals. In such a case it makes no\n",
    "sense to continue to evolve the \"large\" $k$-intervals (which are further along in time) as the actual physical result requires *all* the $k$-intervals at a given time. It is therefore better to put as much computational power as possible into evolving the new $k$-points forward in time. If `N` is large then we can easily harness many cores by using spatial decomposition, and so can use *all* available cores to integrate the new $k$ points up to the \"current time\", at which point we split the effort between all the $k$-intervals again.\n",
    "\n",
    "[*put a flow diagram here -- it's pretty complicated in words alone*]\n",
    "\n",
    "##### VI. `nprocs` $\\ll$ `n_k` and `N` \"small\"\n",
    "This is another possibly common situation. We want to do an integration over\n",
    "the Brillouin zone and we need lots of $k$-points. We will use a similar alorithm to the latter case, except that because `N` is small we cannot get any speedup from using spatial decomposition. We will have to use\n",
    "\n",
    "[*put a flow diagram here -- it's pretty complicated in words alone*]\n",
    "\n",
    "\n",
    "#### Further comments\n",
    "It seems that if we think about the problem in the correct way, then a problem which is initially case (V) or (VI) actually becomes (I) and (II) respectively when we have to \"stop\" everything when a $k$-interval is split.\n",
    "\n",
    "In reality we will always (unless we start on some existing $k$-mesh) start in case (I) or (II), but may attain (III) and (IV) or even (V) and (VI) in the worst case scenario. tkwant will need to be able to detect when we enter these different regimes and act accordingly.\n",
    "\n",
    "The crossover from `N` \"large\" to `N` \"small\" will happen when the communication cost becomes a significant fraction of the total computation cost. This will depend not only on the value of `N` itself, but also on the connectedness of the system."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Domain decomposition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general we have a problem defined on a graph. We have to calculate quantities defined on the vertices (the time-derivative of the wavefunction), and the edges give the dependencies between\n",
    "the values computed at the various vertices. i.e. to calculate the derivative of the wavefunction component on site $i$, we need to know the value of the wavefunction on site $i$ and also all sites connected to $i$ via a hopping.\n",
    "\n",
    "The problem is thus ripe for a domain composition where we partition the graph into $n$ pieces where the number of nodes in each piece is roughly the same and where the number of edges connecting the pieces is minimized. We can then hand off treatment of each of the partitions to a separate core. Each core needs the value of the wavefunction at each vertex in its partition, as well as on each of the neighboring sites in the other partitions.\n",
    "\n",
    "We can use the [SCOTCH](http://www.labri.fr/perso/pelegrin/scotch/) library to do the partitioning for us. We would then have to re-order the sites so that they occur sequentially within the partitions, and then we can trivially use [ODEint's parallel support](https://headmyshoulder.github.io/odeint-v2/doc/boost_numeric_odeint/tutorial/parallel_computation_with_openmp_and_mpi.html).\n",
    "\n",
    "The complications will arise, as detailed above, when we need to switch modes during the calculation to take into account the different needs. This means we will need to \"repurpose\" workers, which will clearly get rather complicated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Handling different parallel environments"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The most basic user may not want to calculate many-body observables. In such a case there should be no usage whatsoever of MPI or 0MQ or anything. In fact, the two parts should be completely separate; these libraries should not be necessary if a user just wants the most basic usage.\n",
    "\n",
    "More advanced users will, however, need this capability and will need it to be well supported. It seems to make sense to have the concept of an `Interconnect` which links together processes. This thing would provide a rather limited interface in order to maintain compatibility with a variety of backends. The `Interconnect` would behave essentially like an MPI-1.1 communicator in the sense that communication operations on it are blocking, and it provides collective communication primitives such as `gather` and `reduce`. The most common usecase would be an `MPIInterconnect`, but we could also imagine having `ZMQInterconnect` or `SocketInterconnect`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The tkwant workers would thus assume an `Interconnect` which allows them to communicate with each other and also with the master. A `Runner` object would then be responsible for setting up an `Interconnect`. Using the example of `MPI`, the `MPIRunner` would be run using `mpirun`:\n",
    "    \n",
    "    mpirun -n 8 python -m tkwant.runners.mpi script.py\n",
    "    \n",
    "and then it would be responsible for setting up the necessary communicators and creating an `MPIInterconnect`, rank 0 would then run the script and the calls which require parallelism can thus assume that they have it.\n",
    "\n",
    "We could also have interactive use:\n",
    "\n",
    "    mpirun -n 8 python -m tkwant.runners.mpi\n",
    "    \n",
    "or even have rank 0 execute an IPython kernel, e.g. on in a batch job:\n",
    "\n",
    "    mpirun -machinefile $PBS_NODEFILE\\\n",
    "    python -m tkwant.runners.mpi_ipython\n",
    "    \n",
    "then we could just go:\n",
    "\n",
    "    ipython console --existing --ssh node\n",
    "    \n",
    "on the master node in order to connect to the kernel and start using tkwant in parallel!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We could also imagine having poor-man's process launching if we don't have MPI, just like:\n",
    "\n",
    "    for i in 1 2 3 4\n",
    "    do\n",
    "        python -m tkwant.runners.worker\n",
    "    done\n",
    "    \n",
    "    python -m tkwant.runners.master script.py\n",
    "    \n",
    "which would then use a `ZMQInterconnect`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "the `Interconnect` will also be used by the workers to send updates to the master as to the progress of the calculation.\n",
    "\n",
    "In addition it will need to be used for communication during the spatial decomposition (if there is any). We will need it to be quite flexible in order to be able to do load balancing. It was noted [here](http://www.mcs.anl.gov/~balaji/pubs/2011/eurompi/eurompi11.pgroups.pdf) that even with current generation MPI we can create communicators via non-collective operations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using the IPython notebook with parallel tkwant"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unfortunately the IPython notebook cannot currently attach to existing kernels.\n",
    "This is [not likely to be fixed soon](https://github.com/ipython/ipython/issues/4066) as it would lead to all kinds of complications; currently there is a strong assumption everywhere that notebooks start their own kernels."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "IPython 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.1+"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
