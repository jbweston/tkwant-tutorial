tkwant tutorial
---------------

This is a set of notebooks that outlines the proposed interface to the `tkwant`
extension to `kwant` for handling time-dependent problems. The aim of the
notebooks is that they flow well enough that they could form the basis for
an eventual `tkwant` tutorial. One should start with `0_intro.ipynb` and
follow the notebooks sequentially.

Although there are other materials in this repository (notably in the
`implementation_details` folder), this is mostly non-pedagogical resources
where I am collecting my thoughts; many of them are incoherent or incomplete.
