{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\newcommand{\\ket}[1]{\\left|{#1}\\right\\rangle}$\n",
    "$\\newcommand{\\bra}[1]{\\left\\langle{#1}\\right|}$\n",
    "$\\newcommand{\\braket}[2]{\\left\\langle{#1}\\,\\right|\\left.{#2}\\right\\rangle}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# IV. Evolving states in time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we have a (possibly time-dependent) tkwant system and an initial state, we next wish to evolve such a state forward in time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%run bean_system.ipy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def plot_spin_densities(tsys, psi_before, psi_after):\n",
    "    spin_density = tkwant.Density(s_z)\n",
    "    fig, (ax1, ax2) = plt.subplots(1, 2, figsize(15, 12))\n",
    "    kwant.plotter.map(tsys, spin_density(psi_before), ax=ax1)\n",
    "    kwant.plotter.map(tsys, spin_density(psi_after), ax=ax2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us start from our previous example of a quantum dot attached to two leads:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "lat, sys = make_system()\n",
    "\n",
    "lead = make_lead(lat)\n",
    "\n",
    "sys.attach_lead(lead)\n",
    "sys.attach_lead(lead.reversed())\n",
    "\n",
    "tsys = sys.finalized()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We construct a localized state and a scattering state,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "args = (0.1, 1.)\n",
    "\n",
    "local_state = tkwant.wave_packet(tsys, args)\n",
    "local_state[:] = bump  # from bean_system.ipy\n",
    "\n",
    "scat_state = tkwant.wave_function(tsys, energy=1., args=args)(0)[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and we have all the ingredients required to evolve in time. In fact, it turns out that the objects returned by\n",
    "`tkwant.wave_function` and `tkwant.wave_packet` already know how to evolve themselves in time:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "local_state_later = local_state(1.)\n",
    "even_later = local_state(2.)\n",
    "\n",
    "scat_state_later = scat_state(1.)\n",
    "even_later = scat_state(2.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "calling these objects with a single number evolves the state forward to the specified time, and returns a copy of the state at this time, which can be indexed as normal using sites."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print(local_state_later[lat(0, 1)])\n",
    "print(scat_state_later[lat(0, 1)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "also the original object can be queried in an analogous way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print(local_state[lat(0, 1)])\n",
    "print(scat_state[lat(0, 1)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "to avoid making a copy we can instead use the `evolve` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "scat_state.evolve(4.)  # returns nothing\n",
    "scat_state[lat(0, 1)]  # query the internal state\n",
    "scat_state.data  # get the raw data array"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and query the internal time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "scat_state.time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the system is *finite* (i.e. no attached leads), then we can also run the evolution back in time:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "local_state.evolve(10.)\n",
    "original_val = local_state(0.)  # go backwards in time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For an *infinite system* our evolution is non-unitary because we only track a certain subset of the degrees\n",
    "of freedom (those in the central region), and so attempting to evolve backwards in time will result in an error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Differences for finite or infinite systems\n",
    "Finite systems are easier to deal with because we can have the wavefunction over\n",
    "the whole Hilbert space of the problem. In such a case we just solve the finite\n",
    "time-dependent Schrödinger equation. For the case of an infinite system we need to take the leads\n",
    "into account. We add a \"sink\" term to the Hamiltonian which corresponds to having\n",
    "an absorbing layer in the leads. For more details see [the appendix](source_sink.ipynb).\n",
    "\n",
    "## Arbitrary states or Eigenstates\n",
    "\"Arbitrary states\" are states with a finite weight on the orbitals of the central,\n",
    "finite region of the full system and 0 weight elsewhere.\n",
    "\"Eigenstates\" in the infinite case are the bound states and scattering states\n",
    "which satisfy a time-independent Schrödinger equation at the initial time $t=0$.\n",
    "$$\n",
    "    \\hat{H}(0) \\ket{\\psi(0)} = E\\ket{\\psi(0)}\n",
    "$$\n",
    "where $\\hat{H}$ is the full Hamiltonian of the infinite system.\n",
    "\n",
    "All states evolve according to a time-dependent Schrödinger equation (TDSE)\n",
    "$$\n",
    "    i\\partial_t\\ket{\\psi(t)} = \\hat{H}(t) \\ket{\\psi(t)}\n",
    "$$\n",
    "however for the case where $|\\psi(0)\\rangle$ is an eigenstate we can choose to solve an equivalent differential equation which turns out to be much simpler.\n",
    "\n",
    "Consider that\n",
    "<div style=\"border: 0.2em #FF9191 solid;\">\n",
    "$$\n",
    "    \\ket{\\psi(t)} = (\\ket{\\phi_n} + \\ket{\\bar{\\psi}(t)})e^{-iE_nt}\n",
    "$$\n",
    "</div>\n",
    "where $\\hat{H}(0) \\ket{\\phi_n} = E_n \\ket{\\phi_n}$ (i.e. the system is initially in an eigenstate at energy $E_n$) and $\\ket{\\bar{\\psi}(0)} = 0$. Then we can write the TDSE as\n",
    "$$\n",
    "e^{-iE_nt}\\left(\n",
    "    E_n\\ket{\\phi_n} + E_n\\ket{\\bar{\\psi}(t)} + i\\partial_t\\ket{\\bar{\\psi}(t)}\n",
    "\\right) = e^{-iEt}\\left(\n",
    "    E_n\\ket{\\phi_n} + \\hat{W}(t)\\ket{\\phi_n} + \\hat{H}(t)\\ket{\\bar{\\psi}(t)}\n",
    "\\right)\n",
    "$$\n",
    "where $\\hat{H}(t) = \\hat{H}_0 + \\hat{W}(t)$. We can simplify this to\n",
    "<div style=\"border: 0.2em #FF9191 solid;\">\n",
    "$$\n",
    "i\\partial_t\\ket{\\bar{\\psi}(t)} =\n",
    "(\\hat{H}(t) - E_n) \\ket{\\bar{\\psi}(t)} + \\hat{W}(t) \\ket{\\phi_n}\n",
    "$$\n",
    "</div>\n",
    "This is simpler than the initial TDSE because $\\ket{\\bar{\\psi}(t)}$ is initially 0 everywhere. In addition, as $\\hat{W}(t)$ *necessarily* only has non-zero matrix elements between orbitals in the central region, this means that $\\ket{\\bar{\\psi}(t)}$ will always propagate *from* the central region *into* the leads.\n",
    "The above equations describe how to propagate eigenstates. For more details see [the appendix](source_sink.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Choosing a Solver and stepper and setting parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above usage is very simple and will work for 99% of use cases, however it is possible that users\n",
    "may wish for more control over the *exact* algorithm used for the time evolution. In the same way that the Kwant\n",
    "functions `kwant.wave_function` and `kwant.smatrix` are wrappers around a default solver, and in principle one\n",
    "can construct such a solver explicitly, setting numerical tolerances etc, tkwant will provide similar functionality.\n",
    "\n",
    "The solver names are very much provisional; we need to come up with better ones!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# The default solver, uses a region of increasing imaginary potential in\n",
    "# the leads. Can also be used with wide-band limit boundary conditions.\n",
    "# This corresponds to the \"source-sink\" algorithm\n",
    "solver = tkwant.solvers.source_sink.Solver()\n",
    "# specify ODE stepper\n",
    "solver.stepper = tkwant.steppers.RK45(atol=1E-12, hmin=1E-8)\n",
    "# specify boundary conditions consisting of an absorbing region,\n",
    "# absorbing potential increases as x**6 over a length of 1000 lead cells\n",
    "solver.boundaries = tkwant.solvers.boundaries.Monomial(order=6, power=20, length=1000)\n",
    "\n",
    "scat_state = solver.wave_function(tsys, energy=1., args=args)(0)[0]\n",
    "scat_state(10.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# this solver adds successive layers to the simulation domain as the simulation\n",
    "# progresses, so that no reflections can ever occur.\n",
    "# This means that computation time and memory usage scales as O(NT).\n",
    "# This corresponds to the WF-C method\n",
    "solver = tkwant.solvers.expanding.Solver()\n",
    "# specify ODE stepper\n",
    "solver.stepper = tkwant.steppers.CrankNicolson(h=1E-3)\n",
    "\n",
    "\n",
    "scat_state = solver.wave_function(tsys, energy=1., args=args)(0)[0]\n",
    "scat_state(10.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# this solver uses the full convolution integral as boundary conditions\n",
    "# runtime and memory usage scale the same as the `expanding` solver,\n",
    "# so this is mainly used just as a \"check\". This corresponds to the WF-B method.\n",
    "solver = tkwant.solvers.convolution.Solver()\n",
    "# specify ODE solver\n",
    "solver.stepper = tkwant.steppers.Dopri5(atol=1E-12, hmin=1E-8)\n",
    "\n",
    "scat_state = solver.wave_function(tsys, energy=1., args=args)(0)[0]\n",
    "scat_state(10.)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.1+"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
