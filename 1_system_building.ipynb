{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# I. Building a time-dependent system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first thing anyone will want to do in order to use tkwant is to specify the system under study.\n",
    "This is made very easy thanks to the kwant Builder; all one need do is specify value functions in\n",
    "their system which depend on a \"time\" parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from math import cos, pi, exp\n",
    "import matplotlib.pyplot as plt\n",
    "import kwant\n",
    "import tkwant"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can create the central part of our system using a Kwant builder, and have arbitrary time-dependence via a `time` parameter to our value functions. The `time` parameter must be the first after the site parameter(s). As this is just creating a regular Kwant system, all the value functions must formally take the same parameters. Because of this constraint, it is impossible for tkwant to know which functions *actually depend on time*; for this reason you must mark the value functions that *actually* depend on time with the `@tkwant.time_dependent` decorator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def make_central_system():\n",
    "    def bean(pos):\n",
    "        x, y = pos\n",
    "        rr = x**2 + y**2\n",
    "        return rr**2 < 15 * y * rr + x**2 * y**2\n",
    "\n",
    "    # onsite depends on time\n",
    "    @tkwant.time_dependent\n",
    "    def onsite(site, time, Vg, gamma):\n",
    "        return (Vg / 2) * (1 - cos(time)) + 4 * gamma\n",
    "\n",
    "    # hopping does not depend on time in this example\n",
    "    def hopping(site_i, site_j, time, Vg, gamma):\n",
    "        return -gamma\n",
    "\n",
    "    lat = kwant.lattice.square()\n",
    "    td_sys = kwant.Builder()\n",
    "    td_sys[lat.shape(bean, (0, 5))] = onsite\n",
    "    td_sys[lat.neighbors()] = hopping\n",
    "\n",
    "    return lat, td_sys"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also build some leads which we will later attach to the central region. \n",
    "Note that tkwant requires that your leads be *formally time independent*. If your\n",
    "Hamiltonian contains time-dependent leads you must first perform a gauge\n",
    "transformation to bring the time-dependence into the coupling between the\n",
    "lead and the central region. We will later see that we can specify the time-dependence afterwards."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def make_leads():\n",
    "    \n",
    "    def lead_hop(i, j, time, Vg, gamma):\n",
    "        return -gamma\n",
    "    \n",
    "    def lead_onsite(i, time, Vg gamma):\n",
    "        return 4 * gamma\n",
    "    \n",
    "    lead= kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))\n",
    "    lead[(lat(0, j) for j in xrange(7, 13))] = lead_onsite\n",
    "    lead[lat.neighbors()] = lead_hopping\n",
    "    \n",
    "    lead2 = lead.reversed()\n",
    "\n",
    "    return lead, lead2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you mark any of the lead value functions with `@tkwant.time_dependent` an error will be raised.\n",
    "\n",
    "Finally we create the system and attach the leads."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "lat, sys = make_central_system()\n",
    "lead1, lead2 = make_leads()\n",
    "sys.attach_lead(lead1)\n",
    "sys.attach_lead(lead2)\n",
    "kwant.plot(sys)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After we have done so we can modify the system so that the time-dependence of the leads *is* taken into account. In the following example we apply a time-dependent voltage to the left-hand lead. Note that the voltage function must take the same parameters as the regular Hamiltonian value functions (but without the site parameter(s))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def voltage(time, Vg, gamma):\n",
    "    return 0.1 * time  # ramping voltage\n",
    "\n",
    "tkwant.leads.add_voltage(sys, 0, voltage)  # takes a system, lead number, and voltage function\n",
    "kwant.plot(sys)  # sys has been modified, and a single lead layer has been added to the system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that after calling `tkwant.leads.add_voltage` our system has extra sites added where the lead joins the system. The hoppings between the sites in this added lead cell and the sites in the rest of the system are the same as the hoppings between lead cells, except that they are multiplied by a time-dependent value, equal to exponential of the Faraday flux due to the applied voltage:\n",
    "$$\n",
    "    \\exp[-i\\phi(t)] \\;,\\; \\phi(t) = \\int_0^t V(t') \\, dt'\n",
    "$$\n",
    "\n",
    "Naturally, as tkwant has to perform the integral manually, this can be kind of slow. If the antiderivative of the\n",
    "voltage applied to the lead has a simple form you can specify it explicitly by using the `phase` keyword. For example,\n",
    "to apply the *same* voltage to the other lead we could also say:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def voltage_phase(time, Vg, gamma):\n",
    "    return 0.5 * 0.1 * time**2\n",
    "\n",
    "tkwant.leads.add_voltage(sys, 1, phase=voltage_phase)\n",
    "kwant.plot(sys)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the case where there is more than one orbital per site, the `voltage` or `voltage_phase` function can return\n",
    "a *matrix* of voltages/phases, which has the same shape as the hoppings from the lead to the central system.\n",
    "In general the new hopping is calculated using\n",
    "```python\n",
    "new_hop = old_hop * numpy.exp(-1j * phase(time, *other_args))\n",
    "```\n",
    "i.e. the return value of the phase function is exponentiated *element-wise*, and it multiplied *element-wise* by\n",
    "the old hopping.\n",
    "\n",
    "For example, if one was treating superconductivity this would allow you to do the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def voltage_phase(time, V_slope):\n",
    "    return 0.5 * V_slope * time**2 * np.array([[1, 0], [0, -1]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which would be the correct way to apply a voltage to the lead, properly taking into account the fact\n",
    "that the electric field acts with a minus sign on the holes with respect to the electrons."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For other types of arbitrary time dependence in the leads, you must explicitly perform the gauge transformation and add the necessary Hamiltonian elements to the central system yourself."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" data-toggle=\"collapse\" data-target=\"#info2\">\n",
    "<p>**Note** <i class=\"pull-right fa fa-chevron-down\"></i></p>\n",
    "<div id=\"info2\">\n",
    "Note that we did not <em>need</em> to use `tkwant.leads.add_voltage` at all! we could very well have defined our central region with the extra cells added and the hopping added manually. `tkwant.leads.add_voltage` just automates this (very) common process.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" data-toggle=\"collapse\" data-target=\"#info3\">\n",
    "<p>**Note** <i class=\"pull-right fa fa-chevron-down\"></i></p>\n",
    "<div id=\"info3\" class=\"collapse\">\n",
    "The `voltage` function does not need to know about the time-independent part of the hopping.\n",
    "In this way time-dependence in the leads can be trivially added to leads with/without magnetic field, spin-orbit etc.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the system can be finalized as normal:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "finalized_sys = sys.finalized()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So in the end we have a *bona-fide kwant system* that we can use with all the machinery of kwant, but also the machinery of `tkwant` due to the `time` parameter."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Convenience functions: making a train of pulses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One common operation that one may want to do is to apply a *train* of voltage\n",
    "pulses with period $T$. tkwant provides a function that can take the voltage\n",
    "defined over one period and replicate it for all $t>0$.\n",
    "\n",
    "For example, if we have a voltage pulse that looks like a bump function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def pulse(t):\n",
    "    if abs(t) < 1:\n",
    "        return exp(-1./(1. - x**2))\n",
    "    else:\n",
    "        return 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "then we can create a train of them with period $4$ like so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pulse_train = tkwant.leads.pulse_train(pulse, [-2, 2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "the first argument is the pulse function itself, and the second argument\n",
    "defines the time window over which the pulse function will be sampled.\n",
    "In addition, the produced train function is offset in time with respect to the\n",
    "original pulse function, as illustrated below"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/functions.svg\" width=100% />"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.1+"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
